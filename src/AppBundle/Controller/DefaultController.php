<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * @Route(service="app.default_controller")
 */
class DefaultController
{
    protected $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/one/{hydrate}", name="bench_one")
     */
    public function oneAction($hydrate)
    {
        $id = 1;
        $startMem = memory_get_usage();
        $stopwatch = new Stopwatch();
        $stopwatch->start('foo');

        $model = $this->doctrine->getRepository('\AppBundle\Entity\News')->getOne($id, $hydrate);

        $event = $stopwatch->stop('foo');

        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        $response = "Time diff: $time ms \n";
        $response .= "Memory diff: $memory kB \n";

        return new Response($response);
    }

    /**
     * @Route("/all/{hydrate}", name="bench_all")
     */
    public function allAction($hydrate)
    {
        $startMem = memory_get_usage();
        $stopwatch = new Stopwatch();
        $stopwatch->start('foo');

        $models = $this->doctrine->getRepository('\AppBundle\Entity\News')->getAll($hydrate);

        $event = $stopwatch->stop('foo');

        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        $response = "Time diff: $time ms \n";
        $response .= "Memory diff: $memory kB \n";

        return new Response($response);
    }

    /**
     * @Route("/join/{hydrate}", name="bench_join")
     */
    public function joinAction($hydrate)
    {
        $id = 1;
        $startMem = memory_get_usage();
        $stopwatch = new Stopwatch();
        $stopwatch->start('foo');

        $models = $this->doctrine->getRepository('\AppBundle\Entity\News')->getCategories($hydrate);

        $event = $stopwatch->stop('foo');

        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        $response = "Time diff: $time ms \n";
        $response .= "Memory diff: $memory kB \n";

        return new Response($response);
    }
}
